import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateFeatureDto } from '../dto/feature/create-feature.dto';
import { UpdateFeatureDto } from '../dto/feature/update-feature.dto';
import { Feature } from '../entitites/feature.entity';

@Injectable()
export class FeaturesService {
  constructor(
    @InjectRepository(Feature)
    private readonly featureRepository: Repository<Feature>
  ) {}

  create(createFeatureDto: CreateFeatureDto) {
    const feature = this.featureRepository.create(createFeatureDto);

    return this.featureRepository.save(feature);
  }

  findAll() {
    return this.featureRepository.findOne();
  }

  findOne(id: number) {
    return this.featureRepository.findOneOrFail(id);
  }

  async update(id: number, updateFeatureDto: UpdateFeatureDto) {
    const feature = await this.featureRepository.preload({
      id,
      ...updateFeatureDto
    });

    if(!feature) {
      throw new NotFoundException('Updated feature not found');
    }

    return this.featureRepository.save(feature);
  }

  async remove(id: number) {
    const feature = await this.findOne(id);

    return this.featureRepository.remove(feature);
  }
}
