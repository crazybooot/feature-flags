import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEnvironmentDto } from '../dto/environment/create-environment.dto';
import { UpdateEnvironmentDto } from '../dto/environment/update-environment.dto';
import { Environment } from '../entitites/environment.entity';

@Injectable()
export class EnvironmentsService {
  constructor(
    @InjectRepository(Environment)
    private readonly environmentRepository: Repository<Environment>
  ) {}

  create(createEnvironmentDto: CreateEnvironmentDto) {
    const environment = this.environmentRepository.create(createEnvironmentDto);

    return this.environmentRepository.save(environment);
  }

  findAll() {
    return this.environmentRepository.find();
  }

  async findOne(id: number) {
    return this.environmentRepository.findOneOrFail(id);
  }

  async update(id: number, updateEnvironmentDto: UpdateEnvironmentDto) {
    const environment = await this.environmentRepository.preload({
      id,
      ...updateEnvironmentDto
    })

    if(!environment) {
      throw new NotFoundException('Updated environment not found');
    }

    return this.environmentRepository.save(environment);
  }

  async remove(id: number) {
    const environment = await this.findOne(id);

    return this.environmentRepository.remove(environment);
  }
}
