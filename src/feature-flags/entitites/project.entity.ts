import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Environment } from "./environment.entity";
import { Feature } from "./feature.entity";

@Entity()
export class Project {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({nullable: true})
    description: string;

    @Column()
    enabled: boolean;

    @OneToMany(type => Environment, environment => environment.project)
    environments: Environment[];

    @OneToMany(type => Feature, feature => feature.project)
    features: Feature[];
}
