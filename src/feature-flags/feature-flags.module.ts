import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Feature } from './entitites/feature.entity';
import { Project } from './entitites/project.entity';
import { Environment } from './entitites/environment.entity';
import { ProjectsController } from './controllers/projects.controller';
import { EnvironmentsController } from './controllers/environments.controller';
import { FeaturesController } from './controllers/features.controller';
import { ProjectsService } from './services/projects.service';
import { EnvironmentsService } from './services/environments.service';
import { FeaturesService } from './services/features.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project, 
      Environment, 
      Feature
    ])
  ],
  controllers: [
    ProjectsController,
    EnvironmentsController,
    FeaturesController
  ],
  providers: [
    ProjectsService,
    EnvironmentsService,
    FeaturesService
  ],
})
export class FeatureFlagsModule {}
 