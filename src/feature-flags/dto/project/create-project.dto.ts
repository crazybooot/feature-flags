import { IsBoolean, IsOptional, IsString } from "class-validator";

export class CreateProjectDto {
    @IsString()
    readonly name: string;

    @IsOptional()
    @IsString()
    readonly descriptions: string;

    @IsBoolean()
    readonly enabled: boolean;
}
