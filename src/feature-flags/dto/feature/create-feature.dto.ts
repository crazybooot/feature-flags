import { IsBoolean, IsOptional, IsString } from "class-validator";

export class CreateFeatureDto {
    @IsString()
    readonly name: string;

    @IsOptional()
    @IsString()
    readonly descriptions: string;

    @IsBoolean()
    readonly enabled: boolean;
}
