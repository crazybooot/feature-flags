import { IsBoolean, IsOptional, IsString } from "class-validator";

export class CreateEnvironmentDto {
    @IsString()
    readonly name: string;

    @IsOptional()
    @IsString()
    readonly descriptions: string;

    @IsBoolean()
    readonly enabled: boolean;
}
