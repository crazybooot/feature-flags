import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FeatureFlagsModule } from './feature-flags/feature-flags.module';
import { ConfigModule, ConfigService } from'@nestjs/config';
import appConfig from './config/app.config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
          type: 'postgres',
          host: configService.get<string>('database.host'),
          port: configService.get<number>('database.port'),
          username: configService.get<string>('database.user'),
          password: configService.get<string>('database.password'),
          database: configService.get<string>('database.name'),
          autoLoadEntities: true,
          synchronize: true,
      }),
    }),
    ConfigModule.forRoot({
      load: [appConfig]
    }),
    FeatureFlagsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
  